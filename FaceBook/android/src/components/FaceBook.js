import React, { Component } from 'react'
import { Text, View, StyleSheet, FlatList, ScrollView } from 'react-native'
import { Avatar, Button, Header, Icon, Input } from 'react-native-elements'
import LeftComponent from './LeftComponent'
import RightComponent from './RightComponent'
import {articles} from './data/articles'
import Story from './Story'
import NewsComponents from './NewsComponents'

export class FaceBook extends Component {
    render() {
        return (
            <View style={styles.container}>

                <Header
                    placement="right"
                    leftComponent={<LeftComponent />}
                    rightComponent={<RightComponent />}
                    backgroundColor="white"
                />
                <ScrollView>
                <View style={{ flexDirection: 'row', height: 50 }}>
                    <Avatar
                        rounded
                        size='medium'
                        source={{ uri: 'https://i.pinimg.com/originals/c1/0f/56/c10f56ea5dcb02b609a1539e15858b35.png' }}
                        containerStyle={{ marginLeft: 10 }}
                    />
                    <Input
                        placeholder="whate's on your mind?"
                        inputContainerStyle={{ borderColor: 'white', }}
                        inputStyle={{ color: 'blue' }}
                    />
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1 }}>
                        <Button
                            titleStyle={{ color: 'blue' }}
                            style={styles.btn}
                            buttonStyle={{ backgroundColor: 'white', }}
                            icon={
                                <Icon
                                    name='videocam'
                                    type='material'
                                    color='red'
                                    size={22}
                                />
                            }
                            title="Video"
                        />
                    </View>

                    
                    <View style={{ flex: 1 }}>
                        <Button
                            titleStyle={{ color: 'blue' }}
                            style={styles.btn}
                            buttonStyle={{ backgroundColor: 'white', }}
                            icon={
                                <Icon
                                    name='image'
                                    type='material'
                                    color='pink'
                                    size={22}
                                />
                            }
                            title="Photo"
                        />
                    </View>
                    <View style={{ flex: 1 }}>
                        <Button
                            style={styles.btn}
                            titleStyle={{color:'blue'}}
                            buttonStyle={{ backgroundColor: 'white', }}
                            icon={
                                <Icon
                                    name='place'
                                    type='material'
                                    color='green'
                                    size={22}
                                />
                            }
                            title="CheckIn"
                        />
                    </View>
                </View>
                <View>
                    <ScrollView style={styles.scrollView}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}>
                       <Story/>
                    </ScrollView>
                </View>
                <View>
                    {
                        articles.map((key) => <NewsComponents key={key.id}
                            url_pro={key.url}
                            profile_name={key.title}
                            description={key.desc}
                            url_show={key.url_post}
                        />
                        )    
                    }     
                   
                </View>
                </ScrollView>
            </View>
        )
    }
}

export default FaceBook

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    row: {
        flex: 1,
        flexDirection: 'row',
    },
    btn: {
        height: 30,
    },
    scrollView: {
        height:130,
        marginRight:5,
        marginLeft:5,
    },
})