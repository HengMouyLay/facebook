import React, { Component } from 'react'
import { Text, View, } from 'react-native'
import { Icon } from 'react-native-elements'



export class RightComponent extends Component {
    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'row',justifyContent:'center',alignItems:'center'}}>
                <Icon
                    name='heart'
                    type='font-awesome'
                    color='lightblue'
                    size={15}
                    containerStyle={{marginRight:5}}
                />
                <Icon
                    name='search'
                    type='font-awesome'
                    color='lightblue'
                    size={15}
                />
            </View>
        )
    }
}

export default RightComponent
