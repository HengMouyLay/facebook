import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class LeftComponent extends Component {
    render() {
        return (
            <View>
                <Text
                    style={{
                        fontSize:30,
                        color:'blue'
                    }}
                > Facebook </Text>
            </View>
        )
    }
}

export default LeftComponent
