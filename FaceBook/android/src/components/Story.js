import React, { Component } from 'react'
import { ImageBackground, Text, View,StyleSheet } from 'react-native'
import { Avatar } from 'react-native-elements';
import { articles } from './data/articles'
import { stories } from './data/stories';

export class Story extends Component {
    render() {
        return (
            <View style={styles.container}>
            {
                    stories.map((key) => <ImageBackground key={key.id}
                        source={{uri:key.url_story}} style={styles.image}
                        imageStyle={{ borderRadius: 10, flex: 1, justifyContent: 'flex-start' }}>
                        <Avatar
                            rounded
                            size='small'
                            source={{ uri: key.url_profile }}
                            containerStyle={{ marginLeft: 5,marginTop:5 }}
                        />
                        <Text style={styles.text}>{key.profile_name}</Text>
                    </ImageBackground> )
            }                
            </View>
        )
    }
}

export default Story
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "row",        
    },
    image: {
        flex: 1,
        resizeMode: "cover",       
        width:100,
        height:130,
        marginLeft:2,
        marginRight:2,
        
    },
    text: {
        color: "white",
        fontSize: 15,
        fontWeight: "bold",
        textAlign: "center",
        marginTop:50,
    }
});
