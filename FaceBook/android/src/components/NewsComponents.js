import React, { Component } from 'react'
import { ActivityIndicator, Dimensions, RecyclerViewBackedScrollView, StyleSheet, Text, View } from 'react-native'
import { Avatar, Icon, Image } from 'react-native-elements'

export class NewsComponents extends Component {
    render() {
        return (
            <View style={{ flexDirection: 'column' }}>
                <View style={styles.container}>
                    <Avatar
                        rounded
                        size='medium'
                        source={{ uri: this.props.url_pro }}
                        containerStyle={{ marginLeft: 10 }}
                    />
                    <View>
                        <Text style={styles.text_style}>{this.props.profile_name}</Text>
                        <Text style={{marginLeft:5,}} >3h</Text>
                    </View>
                    
                    <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'flex-end', marginRight: 10 }}>
                        <Icon
                            name='ellipsis-h'
                            type='font-awesome'
                            color='gray'
                        />
                    </View>
                </View>

                <View >
                    <Text style={{margin:10}}>{this.props.description}</Text>
                    <Image
                        resizeMode="stretch"
                        source={{ uri: this.props.url_show }}
                        style={{ width: Dimensions.get('screen').width, height: 200 }}
                        PlaceholderContent={<ActivityIndicator />}
                    />
                </View>
                <View style={{ flexDirection: 'row', marginRight: 10 }}>
                    <View style={{ flex: 1, alignItems: 'flex-start' }}>
                        <Text style={{margin:5}} >Messager</Text>
                    </View>
                    <View style={{ alignItems: 'flex-end',margin:5 }}>
                        <Icon
                            name='bookmark'
                            type='font-awesome'
                            color='gray'
                        />
                    </View>
                </View>
                <View style={{ flexDirection: 'row', marginRight: 10, justifyContent: 'center',margin:5 }}>
                    <View style={{flex:1,alignItems:'flex-start',flexDirection:'row'}}>
                        <Icon
                            name='heart'
                            type='font-awesome'
                            color='gray'
                        />
                        <Text> 30k</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row'}}>
                        <Icon
                            name='comment-bank'
                            type='material'
                            color='gray'
                        />
                        <Text> 30k</Text>
                    </View>
                    <View style={{ flex: 1,alignItems:'flex-end' }}>
                        <Text >100000 Share</Text>
                    </View>
                </View>
            </View>
        )
    }
}

export default NewsComponents
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginTop: 2,
    },
    text_style:
    {
        fontWeight: 'bold',
        marginTop:5,
        marginLeft:5,
        fontFamily:'KHMEROSCONTENT',
    }
})